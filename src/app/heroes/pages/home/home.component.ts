import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Auth } from 'src/app/auth/interfaces/auth.interface';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
    `
      .icon{   vertical-align: middle;   margin-right: 10px;   }
      .container{  margin: 10px;  }
      .space { margin: 0 10px; }
    `
  ]
})
export class HomeComponent {

  get auth(): Auth {
    return this.authService.auth;
  } 
  

  constructor(private _route: Router,
              private authService: AuthService ) {  }
  
  ngOnInit(): void {
    
  }

  loggout() {
    this.authService.loggout();
    this._route.navigate(['./auth']);
  }
}
