import { Component, OnInit } from '@angular/core';
import { Heroe, Publisher } from '../../interfaces/heroes.interface';
import { HeroesService } from '../../services/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmarComponent } from '../../components/confirmar/confirmar.component';
import { empty, of } from 'rxjs';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styles: [`
    .grid{  display: grid;   box-sizing: border-box;}
    .gap{  gap: 10px;  }
    .g-colum-50{ grid-template-columns: calc(50% - 0px ) calc(50% - 0px);}
    .w-100{  width: 100%;  }
    .grid-in{  grid-template-columns: calc(50% - 5px) calc(50% - 5px); }
    @media (max-width: 580px){ 
      .grid-in{  grid-template-columns: calc(100% - 5px); }
    }
    .j-i{ justify-items: center;}
    img{ width: 25vw; border-radius: 25px}
    .flex{  display: flex;  }
  `]
})
export class AgregarComponent implements OnInit{
  publishers = [
    {
      id: 'DC Comics',
      desc: 'DC - Comics'
    },
    {
      id: 'Marvel Comics',
      desc: 'Marvel - Comics'
    }
  ];

  heroe: Heroe = {
    superhero: '',
    alter_ego: '',
    characters: '',
    first_appearance: '',
    publisher: Publisher.DCComics,
    alt_img: '',
  }

  constructor(private heroeService: HeroesService,
              private activatedRoute: ActivatedRoute,
              private route: Router,
              private _snackbar: MatSnackBar,
              private _dialog: MatDialog ) {}

  ngOnInit(): void {
    
    if( !this.route.url.includes('editar')){
      return;
    }
    
    this.activatedRoute.params
      .pipe(
        switchMap( ({id}) => this.heroeService.getHeroeById( id ) )
      )
      .subscribe({
        next: (heroe) => this.heroe = heroe, 
      });    
  }
  
  guardar(){
    if( this.heroe.superhero.trim().length === 0){
      return;
    }
    
    if( this.heroe.id ) {
      //Actualizando
      this.heroeService.actualizarHeroe(this.heroe)
        .subscribe({
          next: (heroe) => {
            this.route.navigate(['/heroes', heroe.id]);
            this.mostrarSnackBar('Registro Actualizado');
          }
        })
    } else {
      //Creando
      this.heroeService.agregarHeroe(this.heroe)
      .subscribe({
        next: (heroe) => {
          this.route.navigate(['/heroes', heroe.id]);
          this.mostrarSnackBar('Registro Creado');
        },
      })
    }
  }

  borrar() {
    // Confirmación
    const dialog = this._dialog.open(ConfirmarComponent, {
      data: {...this.heroe},
    });

    dialog.afterClosed()
      .pipe(
        switchMap( result => {
          if(result){
            return this.heroeService.deleteHeroe(this.heroe.id!);
          }
          console.log(result);
          return of(null);
        }
      ))
      .subscribe({
        next: (result) => {
          if(result){
            this.route.navigate(['/heroes']);
            console.log('llego');
            this.mostrarSnackBar('Héroe Eliminado');
          }
        },
        error: err => console.info(err)
        
      })
    // Eliminar
    /*
    this.heroeService.deleteHeroe(this.heroe.id!)
      .subscribe({
        next: (res) => this.route.navigate(['/heroes']),
      });*/
  }


  mostrarSnackBar(mensaje: string):void {
    this._snackbar.open(mensaje, 'ok!', {
      duration: 4500,
    })
  }
}
