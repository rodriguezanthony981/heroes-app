import { Component, OnInit } from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Heroe } from '../../interfaces/heroes.interface';
import { HeroesService } from '../../services/heroes.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styles: [`
    .w-100{  width: 100%  }
    .item-a {  grid-area: header; }
    .item-b {  grid-area: main;  }
    .item-c {  grid-area: sidebar;  }
    .grid{
      display: grid;
      grid-template-columns: 50vw  50vw;
      grid-template-rows: auto;
      grid-template-areas: 
        "header header"
        "main sidebar"
        " . sidebar";
        width:auto;
      overflow: hidden;
    }

  `]
})
export class BuscarComponent implements OnInit {
  termino: string = '';
  heroes: Heroe[] = [];
  heroeSeleccionado!: Heroe;

  constructor(private heroesService: HeroesService ) { }

  ngOnInit(): void {
    
  }

  buscando() {
    this.heroesService.getSugerencias(this.termino.trim() )
      .subscribe({
        next: heroes => {
          this.heroes = heroes
          console.log(heroes)
        },
        error: err => console.log(err),
        complete: () => {
          
        }
      });
  }

  opcionSeleccionada(event: MatAutocompleteSelectedEvent){  
    if(event.option.value === ''){
      return ;
    }

    const heroe: Heroe = event.option.value;
    this.termino = heroe.superhero;

    this.heroesService.getHeroeById( heroe.id! )
      .subscribe({
        next: heroe => this.heroeSeleccionado =  heroe
      });
    
  }
}
