import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../services/heroes.service';
import { Heroe } from '../../interfaces/heroes.interface';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styles: [` 
    .spinner{  margin: 6rem auto;  }
  `]
})
export class ListadoComponent implements OnInit{
  
  heroes!: Heroe[];

  constructor(private heroeService: HeroesService) { }

  ngOnInit(): void {
    this.heroeService.getHeroes()
    .subscribe({
      next: (res) => this.heroes = res,
      error: (err) => console.error(err)
    });    
  }

}
