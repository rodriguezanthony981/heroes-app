import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Heroe } from '../../interfaces/heroes.interface';
import { HeroesService } from '../../services/heroes.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: [`
    .grid {
      display: grid;
      grid-template-columns: repeat(2, minmax(0, 50vw));
      gap: 10px;
    }    
    img{  width: 100%; padding: 0 20px; box-sizing: border-box; border-radius: 20px}
    @media (max-width: 580px){
      .grid { grid-template-columns: repeat(1, minmax(0, 100vw)); }
    }
  `]
})
export class HeroeComponent implements OnInit {
  
  heroe!: Heroe;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private HeroesService: HeroesService, 
    private router: Router ) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        switchMap( ({ id }) => this.HeroesService.getHeroeById(id))
      )
      .subscribe({
        next: ( heroe ) => this.heroe = heroe,
        error: (err) => console.log(err)
      });
  }

  btnregresar(){
    this.router.navigate(['/heroes/listado']);
  }
}
