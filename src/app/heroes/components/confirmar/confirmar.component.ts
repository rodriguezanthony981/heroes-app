import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Heroe } from '../../interfaces/heroes.interface';

@Component({
  selector: 'app-confirmar',
  templateUrl: './confirmar.component.html',
  styles: [`
    .dialog{  padding: 30px; width: 30vw; color: white;}
    .flex{  display: flex;  }
  `]
})
export class ConfirmarComponent {

  constructor( private _dialogoRef: MatDialogRef<ConfirmarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Heroe ) { }
  
  borrar() {
    this._dialogoRef.close(true);
  }

  cerrar() {
    this._dialogoRef.close();
  }
}
