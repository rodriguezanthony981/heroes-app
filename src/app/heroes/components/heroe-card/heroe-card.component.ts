import { Component, Input } from '@angular/core';
import { Heroe } from '../../interfaces/heroes.interface';

@Component({
  selector: 'app-heroe-card',
  templateUrl: './heroe-card.component.html',
  styles: [`
    .grid{  margin-top: 20px; display: grid; gap: 20px;  }
    .width{ grid-template-columns: repeat(1, minmax(0, 40vw)) !important;}
    @media (min-width: 1400px) { 
      .grid{ grid-template-columns: repeat(6, minmax(0, 20vw)); }
    }
    @media (max-width: 1400px) { 
      .grid{ grid-template-columns: repeat(6, minmax(0, 20vw)); }
    }
    @media (max-width: 1200px) { 
      .grid{ grid-template-columns: repeat(5, minmax(0, 20vw));  }
    }
    @media (max-width: 1025px) {
      .grid{ grid-template-columns: repeat(4, minmax(0, 25vw));  }
    }        
    @media (max-width: 768px) {
      .grid{ grid-template-columns: repeat(3, minmax(0, 33vw));  }
    }
    @media (max-width: 580px){
      .grid{ grid-template-columns: repeat(2, minmax(0, 50vw));  }
    }
    @media (max-width: 250px){ 
      .grid{ grid-template-columns: repeat(1, minmax(0, 100vw)); }
    }
    .header {  grid-area: header;  }
    .img {  grid-area: image;  }
    .content {  grid-area: content; }
    .actions {  grid-area: actions;}
    .gridC{
      width: 45vw;
      margin: 20px;
      display: grid;
      grid-template-columns: 50%  30%;
      grid-template-rows: auto;
      grid-template-areas: 
        "header image"
        "content image"
        "actions image";
      overflow: hidden;
    }
    img{
      margin: 10px 0;
    }
  `]
})
export class HeroeCardComponent {

  @Input() heroes: Heroe[] = [];
  @Input() type: boolean = false;
}
