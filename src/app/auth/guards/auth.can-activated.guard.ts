import { inject } from "@angular/core"
import { CanActivateFn } from '@angular/router';
import { AuthService } from "../services/auth.service";
import { Router } from "@angular/router";
import { tap } from 'rxjs';

export const authCanActivatedGuard: CanActivateFn = (route, state) => {
  const authService = inject(AuthService);
  
  const routed = inject(Router);
   
  console.log('Bloqueado por el AuthGuard - CanActivated')
  return authService.verificaAuthentication()
          .pipe(
            tap( estaAutenticado => {
              if(!estaAutenticado){
                routed.navigate(['./auth/login']);
              }
             })
          );
};
