import { inject } from '@angular/core';
import { CanMatchFn } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { tap } from 'rxjs';

export const authCanMatchGuard: CanMatchFn = (route, segments) => {
  //Se debe injectar la clase del servicio y usarlo como variable  
  const authService = inject(AuthService);
  const routed = inject(Router);
   
  return authService.verificaAuthentication()
          .pipe(
            tap( estaAutenticado => {
              if(!estaAutenticado){
                routed.navigate(['./auth/login']);
              }
             })
          );
};
