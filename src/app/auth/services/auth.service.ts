import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.dev';
import { Auth } from '../interfaces/auth.interface';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl: string = environment.baseUrl;
  private _auth: Auth | undefined;

  get auth(): Auth {
    return { ...this._auth! }
  }

  constructor( private http: HttpClient ) { }

  verificaAuthentication(): Observable<boolean>{
    if( !localStorage.getItem('token') ){
      console.log('Bloequeado por el Guard');
      return of(false);
    }
    return this.http.get<Auth>(`${this.baseUrl}/usuarios/1`)
              .pipe(
                map((auth) => {
                  console.log('auth', auth);
                  this._auth = auth;
                  return true;
                })
              );
  }

  login() {
    return this.http.get<Auth>(`${this.baseUrl}/usuarios/1`)
            .pipe(
              tap( auth => this._auth = auth ),
              tap( auth => localStorage.setItem('token', auth.id) )
            ); 
  }

  loggout() {
    this._auth = undefined;
  }

  clear() {
    localStorage.clear()
  }
  
}
