import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent {
  constructor( private _route: Router,
               private _authService: AuthService ) { }

  login() {
    //Ir al backend Confirmar q exista
    //Usuario
    this._authService.login()
      .subscribe({
        next: (res) => {
          if( res.id ) {
            this._route.navigate(['./heroes']);//this._route.navigate(['./heroes']);
          }
        }
      })
  }

  loggout() {
    this._authService.loggout();
    this._route.navigate(['./heroes']);
  }

  clear() {
    this._authService.clear();
  }
}
